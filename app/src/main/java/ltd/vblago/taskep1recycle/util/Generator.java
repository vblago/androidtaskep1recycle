package ltd.vblago.taskep1recycle.util;

import java.util.ArrayList;
import ltd.vblago.taskep1recycle.R;
import ltd.vblago.taskep1recycle.model.Number;

public class Generator {
    private static ArrayList<Number> numbers = new ArrayList<>();

    public static ArrayList<Number> getNumbers(){

        for (int i = 1; i <= 9; i++) {
            String name = "Name "+ i;
            String email = i + "email@gmail.com";
            String address = "Address " + i;
            String number = "+38050123456" + i;
            numbers.add(new Number(name, email, address, number));
        }

        numbers.get(0).setImage(R.drawable.image1);
        numbers.get(1).setImage(R.drawable.image2);
        numbers.get(2).setImage(R.drawable.image3);
        numbers.get(3).setImage(R.drawable.image4);
        numbers.get(4).setImage(R.drawable.image5);
        numbers.get(5).setImage(R.drawable.image6);
        numbers.get(6).setImage(R.drawable.image7);
        numbers.get(7).setImage(R.drawable.image8);
        numbers.get(8).setImage(R.drawable.image9);
        return numbers;
    }
}
