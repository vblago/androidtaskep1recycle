package ltd.vblago.taskep1recycle.util;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ltd.vblago.taskep1recycle.R;
import ltd.vblago.taskep1recycle.model.Number;

public class NumberAdapter extends RecyclerView.Adapter<NumberAdapter.ViewHolder> {

    private ArrayList<Number> numbers;
    private Context context;
    private Callback callback;

    public NumberAdapter(ArrayList<Number> numbers, Context context) {
        this.numbers = numbers;
        this.context = context;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name_view)
        TextView nameView;
        @BindView(R.id.image_view)
        ImageView imageView;
        @BindView(R.id.email_view)
        TextView emailView;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Number number){
            nameView.setText(number.getName());
            imageView.setImageResource(number.getImage());
            emailView.setText(number.getEmail());
        }

        @OnClick(R.id.item_layout)
        public void onClick(){
            if (callback != null){
                callback.onItemClick(getAdapterPosition());
            }
        }
    }

    public interface Callback {
        void onItemClick(int position);
    }

    public void setCallback(Callback callback){
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(numbers.get(position));
    }

    @Override
    public int getItemCount() {
        return numbers.size();
    }


}
