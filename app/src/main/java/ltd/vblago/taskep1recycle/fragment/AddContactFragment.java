package ltd.vblago.taskep1recycle.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ltd.vblago.taskep1recycle.R;
import ltd.vblago.taskep1recycle.model.Number;
import ltd.vblago.taskep1recycle.util.ActivityCommunication;

public class AddContactFragment extends Fragment {

    @BindView(R.id.editText) EditText nameEdit;
    @BindView(R.id.editText2) EditText numberEdit;
    @BindView(R.id.editText3) EditText emailEdit;
    @BindView(R.id.editText4) EditText addressEdit;
    @BindView(R.id.editText5) EditText imageEdit;

    Unbinder unbinder;

    ActivityCommunication activityCommunication;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ActivityCommunication) {
            activityCommunication = (ActivityCommunication) context;
        } else {
            throw new RuntimeException(Context.class.getSimpleName() + " must implement ActivityCommunication interface");
        }
    }

    public static AddContactFragment newInstance() {
        return new AddContactFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_contact, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @OnClick(R.id.button)
    public void saveNumber(){
        Number number = new Number(nameEdit.getText().toString(),
                emailEdit.getText().toString(),
                addressEdit.getText().toString(),
                numberEdit.getText().toString());
//        String str = "image" + imageEdit.getText().toString();
//        int id = Integer.parseInt(str);
//        number.setImage(id);
        activityCommunication.addNumber(number);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
