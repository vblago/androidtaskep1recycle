package ltd.vblago.taskep1recycle.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ltd.vblago.taskep1recycle.R;
import ltd.vblago.taskep1recycle.model.Number;

public class DetailFragment extends Fragment {

    Number number;
    Unbinder unbinder;

    @BindView(R.id.image_view) ImageView imageView;
    @BindView(R.id.name_view) TextView nameView;
    @BindView(R.id.number_view) TextView numberView;
    @BindView(R.id.email_view) TextView emailView;
    @BindView(R.id.address_view) TextView addressView;


    public static DetailFragment newInstance(Number number) {
        Bundle args = new Bundle();
        args.putSerializable("number", number);
        DetailFragment fragment = new DetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_detail, container, false);
        unbinder = ButterKnife.bind(this, root);
        number = (Number) getArguments().getSerializable("number");

        imageView.setImageResource(number.getImage());
        nameView.setText(number.getName());
        numberView.setText(number.getNumber());
        emailView.setText(number.getEmail());
        addressView.setText(number.getAddress());

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
