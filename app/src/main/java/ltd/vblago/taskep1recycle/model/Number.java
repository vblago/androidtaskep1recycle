package ltd.vblago.taskep1recycle.model;

import java.io.Serializable;

public class Number implements Serializable{
    private String name;
    private String email;
    private String address;
    private String number;
    private int image;

    public Number(String name, String email, String address, String number) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.number = number;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName(){return name;}

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getNumber() {
        return number;
    }

    public int getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
